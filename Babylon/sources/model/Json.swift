//
//  Json.swift
//  Babylon
//
//  Created by verec on 05/08/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import CoreLocation

import UIKit

protocol JsonParsable {
    init?(json:[String:AnyObject])
}

struct Json {

    struct User : JsonParsable {

        struct Address {
            let street:         String
            let suite:          String
            let city:           String
            let zipcode:        String
            let geo:            CLLocationCoordinate2D

            init?(json:[String:AnyObject]) {
                guard let street    = json["street"]    as? String else {    return nil  }
                guard let suite     = json["suite"]     as? String else {    return nil  }
                guard let city      = json["city"]      as? String else {    return nil  }
                guard let zipcode   = json["zipcode"]   as? String else {    return nil  }

                guard let geo       = json["geo"]       as? [String:String]
                    , let latO      = geo["lat"]
                    , let lat       = latO.asDouble
                    , let lngO      = geo["lng"]
                    , let lng       = lngO.asDouble     else {    return nil  }

                self.street     =   street
                self.suite      =   suite
                self.city       =   city
                self.zipcode    =   zipcode
                self.geo        =   CLLocationCoordinate2D(latitude: lat, longitude: lng)
            }
        }

        struct Company {
            let name:           String
            let catchPhrase:    String
            let bs:             String

            init?(json:[String:AnyObject]) {
                guard let name          = json["name"]          as? String else {    return nil  }
                guard let catchPhrase   = json["catchPhrase"]   as? String else {    return nil  }
                guard let bs            = json["bs"]            as? String else {    return nil  }

                self.name = name
                self.catchPhrase = catchPhrase
                self.bs = bs
            }
        }

        let id:                 Int
        let name:               String
        let username:           String
        let email:              String
        let address:            Address
        let phone:              String
        let website:            String
        let company:            Company

        init?(json:[String:AnyObject]) {

            guard let id        = json["id"]        as? Int     else {    return nil  }
            guard let name      = json["name"]      as? String  else {    return nil  }
            guard let username  = json["username"]  as? String  else {    return nil  }
            guard let email     = json["email"]     as? String  else {    return nil  }
            guard let addressJ  = json["address"]   as? [String:AnyObject] else { return nil   }
            guard let address   = Address(json: addressJ)       else { return nil }
            guard let phone     = json["phone"]     as? String  else {    return nil  }
            guard let website   = json["website"]   as? String  else {    return nil  }
            guard let companyJ  = json["company"]   as? [String:AnyObject] else { return nil   }
            guard let company   = Company(json: companyJ)       else { return nil }

            self.id         = id
            self.name       = name
            self.username   = username
            self.email      = email
            self.address    = address

            self.phone      = phone
            self.website    = website
            self.company    = company
        }
    }

    struct Post : JsonParsable {
        let id:         Int
        let userId:     Int
        let title:      String
        let body:       String

        init?(json:[String:AnyObject]) {

            guard let id        = json["id"]        as? Int     else {    return nil  }
            guard let userId    = json["userId"]    as? Int     else {    return nil  }
            guard let title     = json["title"]     as? String  else {    return nil  }
            guard let body      = json["body"]      as? String  else {    return nil  }

            self.id     = id
            self.userId = userId
            self.title  = title
            self.body   = body
        }
    }

    struct Comment : JsonParsable {
        let id:         Int
        let postId:     Int
        let name:       String
        let email:      String
        let body:       String

        init?(json:[String:AnyObject]) {

            guard let id        = json["id"]        as? Int     else {    return nil  }
            guard let postId    = json["postId"]    as? Int     else {    return nil  }
            guard let name      = json["name"]      as? String  else {    return nil  }
            guard let email     = json["email"]     as? String  else {    return nil  }
            guard let body      = json["body"]      as? String  else {    return nil  }

            self.id     = id
            self.postId = postId
            self.email  = email
            self.name   = name
            self.body   = body
        }
    }
}

extension Json {

    static func parse<T:JsonParsable> (json:[[String:AnyObject]]) -> [T] {
        var records = [T]()

        for jsonRecord in json {
            if let record = T(json: jsonRecord) {
                records.append(record)
            }
        }
        return records
    }

    static func decode(jsonData: NSData?) -> [[String:AnyObject]]? {
        if let jsonData = jsonData {
            do {
                if let decoded = try NSJSONSerialization.JSONObjectWithData(jsonData, options: [.AllowFragments]) as? [[String:AnyObject]] {
                    return decoded
                }
            } catch {
                print("error decoding json: \(error)")
            }
        }
        return .None
    }

    static func bundleLoad(kind:String, inBundle: NSBundle = NSBundle.mainBundle()) -> NSData? {
        if let path = inBundle.pathForResource(kind, ofType: "json", inDirectory: "static") {
            let url = NSURL.fileURLWithPath(path)
            return NSData(contentsOfURL: url)
        }

        return .None
    }
}