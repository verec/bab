//
//  Model.swift
//  Babylon
//
//  Created by verec on 05/08/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation

struct Model {

    /// PostCell is the UI side, that which gets fed to tableview/cells
    class PostCell {
        let post: Json.Post
        let user: Json.User
        let cmts: [Json.Comment]
        var bodyVisible:Bool = false
        var userVisible:Bool = false

        init(post: Json.Post, user:Json.User, cmts: [Json.Comment]) {
            self.post = post
            self.user = user
            self.cmts = cmts
        }
    }

    var posts = [PostCell]() {
        didSet {
            print("new \(count) posts")
            WellKnown.model = self
            Views.Post.postView.reloadContents()
        }
    }

    var count: Int {
        return posts.count
    }

    subscript(index:Int) -> PostCell? {

        get {
            if count > index {
                return posts[index]
            }
            return .None
        }

        set (newValue) {
            if let newValue = newValue where count > index {
                posts[index] = newValue
                Views.Post.postView.reload(index)
            }
        }
    }
}

extension Model {

    mutating func ensureLoaded() {
        assert(NSThread.isMainThread())

        if count > 0 {
            return
        }

        self.posts = []

        /// try and fetch from the cache, hit the server if cache empty
        GCD.SerialQueue1.async {

            Views.mainView.setBusy(true)

            var jsonComments:[Json.Comment]?    = .None
            var jsonPosts:[Json.Post]?          = .None
            var jsonUsers:[Json.User]?          = .None

            func loadComments(data: NSData) {
                if let dict = Json.decode(data) {
                    jsonComments = Json.parse(dict)
                }
            }

            func loadPosts(data: NSData) {
                if let dict = Json.decode(data) {
                    jsonPosts = Json.parse(dict)
                }
            }

            func loadUsers(data: NSData) {
                if let dict = Json.decode(data) {
                    jsonUsers = Json.parse(dict)
                }
            }

            var storeNames: [(nane:String, url:String, slurp: (NSData)->())] {
                return [    /// order is important!
                    ("comments",    "http://jsonplaceholder.typicode.com/comments", loadComments)
                ,   ("posts",       "http://jsonplaceholder.typicode.com/posts", loadPosts)
                ,   ("users",       "http://jsonplaceholder.typicode.com/users", loadUsers)
                ]
            }

            let documentPath = "~/Documents".NS.stringByExpandingTildeInPath
            var loadedData = [String:NSData]()

            for (name, _, _) in storeNames {
                let path = documentPath.NS.stringByAppendingPathComponent(name).NS.stringByAppendingPathExtension("json")
                if let path = path, let data = NSData(contentsOfFile: path) {
                    loadedData[name] = data
                }
            }

            func merge() {

                /// FIXME: this runs in O(n^3)!! but n is small ...
                GCD.SerialQueue1.async {

                    var posts = [PostCell]()
                    GCD.MainQueue.sync {
                        posts = self.posts
                    }

                    if posts.count > 0 {
                        print("merge ignored")
                        return
                    }

                    print("merge called")
                    for (name, _, load) in storeNames {
                        if let data = loadedData[name] {
                            load(data)
                        }
                    }

                    if let jsonComments = jsonComments
                    ,  let jsonPosts = jsonPosts
                    ,  let jsonUsers = jsonUsers {

                        print("merge allowed")

                        var posts = [PostCell]()

                        /// we're all set for the big merge
                        for jsonPost in jsonPosts {

                            let cmts = jsonComments.filter {
                                let cmt = $0
                                return cmt.postId == jsonPost.id
                            }

                            let users = jsonUsers.filter {
                                let usr = $0
                                return usr.id == jsonPost.userId
                            }

                            if let user = users.last {
                                let postCell = PostCell(post: jsonPost, user: user, cmts: cmts)
                                posts.append(postCell)
                            } else {
                                print("sad path, one or more of the data set couldn't be retreived")
                            }
                        }

                        /// Make a _sync_ call: we cannot touch the UI
                        /// side from any thread other than the main thread
                        print("merge completing")
                        GCD.MainQueue.sync {
                            self.posts = posts

                            Views.mainView.setBusy(false)
                        }
                    }
                }
            }

            if loadedData.count == storeNames.count {
                merge()
                return
            }

            /// one or more data were not cached. Go to the network
            for (name, url, _) in storeNames {
                GCD.SerialQueue1.async {
                    if let url = NSURL(string: url) {
                        WellKnown.network.load(fullURL: url) {
                            (error, data) in

                            assert(NSThread.isMainThread())

                            if let error = error {
                                /// TODO deal with no cache + not network!
                                print("sad path, one or more of the data set couldn't be retreived")
                                print("\(error)")
                            } else {
                                guard let path = documentPath.NS.stringByAppendingPathComponent(name).NS.stringByAppendingPathExtension("json") else {
                                    print("sad path, one or more of the data set couldn't be retreived")
                                    return
                                }

                                guard let data = data else {
                                    print("sad path, one or more of the data set couldn't be retreived")
                                    return
                                }
                                /// Just serialize so that all branches go through the same code path
                                GCD.SerialQueue1.async {
                                    /// store in the cache
                                    data.writeToFile(path, atomically: true)
                                    loadedData[name] = data

                                    /// merge won't complete until everything is loaded
                                    merge()
                                }
                            }
                        }
                    } else {
                        print("sad path, one or more of the data set couldn't be retreived")
                        print("bug!")
                    }
                }
            }
        }
    }
}