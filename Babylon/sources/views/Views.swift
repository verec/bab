//
//  Views.swift
//  Babylon
//
//  Created by verec on 05/08/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

struct Views {

    static let globalWaitSpinner    = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
    static let mainView             = AppView()

    struct Post {
        static let postView         = PostView()
    }
}