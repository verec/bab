//
//  PostView.swift
//  Babylon
//
//  Created by verec on 07/08/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

class PostView : UIView {

    var tableView: UITableView?

    private struct Parameters {
        static let cellClass                = PostCell.self
        static let cellIdentifier           = NSStringFromClass(cellClass)
    }

    lazy var maxRows:Int = {
        /// TODO: us actual screen size to infer appropriate number of rows
        return 7
    }()

    /// public
    convenience init() {
        self.init(frame:CGRect.zero)
    }

    override init(frame:CGRect) {
        super.init(frame:frame)

        self.tableView = UITableView(frame: self.bounds, style: .Plain)
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        self.tableView?.registerClass(Parameters.cellClass, forCellReuseIdentifier: Parameters.cellIdentifier)

        self.tableView?.showsHorizontalScrollIndicator = false
        self.tableView?.showsVerticalScrollIndicator = true
        self.tableView?.separatorStyle = .None

        self.tableView?.backgroundColor = UIColor.clearColor()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension PostView {

    override func layoutSubviews() {

        super.layoutSubviews()

        guard let tableView = self.tableView else {
            return
        }

        if tableView.orphaned {
            self.addSubview(tableView)
        }

        let insetX = CGFloat(30.0)
        let insetY = CGFloat(30.0)

        let mid = self.bounds.insetBy(dx: insetX, dy: insetY)

        tableView.frame = mid
    }
}

extension PostView {

    func reloadContents() {
        self.tableView?.reloadSections(NSIndexSet(index:0), withRowAnimation: .None)
    }

    func reload(index: Int) {
        if let visibleRows = self.tableView?.indexPathsForVisibleRows {
            let ips = visibleRows.filter { $0.row == index }
            /// make sure there's at least one row before refreshing the first
            if let ip = ips.first {
                self.tableView?.reloadRowsAtIndexPaths([ip], withRowAnimation: .None)
            }
        }
    }
}

extension PostView {

    func bind(cellValue: Model.PostCell?, cell: PostCell, indexPath: NSIndexPath) {

        cell.view.cell = cellValue

        cell.view.autoExpand = {
            [weak self] in
            /// toggle off any other cell that is currently expanded.
            var toReload = [NSIndexPath]()
            for i in 0 ..< WellKnown.model.count where i != indexPath.row {
                if let cell = WellKnown.model[i] {
                    let any = cell.bodyVisible || cell.userVisible
                    if !any {
                        continue
                    }
                    cell.bodyVisible = false
                    cell.userVisible = false
                    let ip = NSIndexPath(forRow:i, inSection: 0)
                    toReload.append(ip)

                    /// Works around a tableview bug whereby the cell whose bounds
                    /// is being changed oought to have its layout called but doesn't
                    if let c = self?.tableView?.cellForRowAtIndexPath(ip) as? PostCell {
                        let it = c.view.cell
                        c.view.cell = it
                    }
                }
            }
            toReload.append(indexPath)
            self?.tableView?.reloadRowsAtIndexPaths(toReload, withRowAnimation: .Automatic)
            self?.tableView?.scrollToRowAtIndexPath(indexPath, atScrollPosition: .None, animated: true)
        }

        cell.view.collapsedHeight = {
            [weak self] in
            return (self?.fixedRowHeight())!
        }
    }
}


extension PostView : UITableViewDelegate {

    func fixedRowHeight() -> CGFloat {
        if let tableView = tableView {
            return tableView.bounds.height / CGFloat(maxRows)
        }
        return 0.0
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var multiplier:CGFloat = 1.0
        if let cellValue = WellKnown.model[indexPath.row] {
            if cellValue.bodyVisible || cellValue.userVisible {
                multiplier = 3.0
            }
        }
        return fixedRowHeight() * multiplier
    }
}

extension PostView : UITableViewDataSource {

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return WellKnown.model.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCellWithIdentifier(Parameters.cellIdentifier) as? PostCell else {
            assert(false, "This just cannot happen")
            return UITableViewCell()
        }
        
        if let cellValue = WellKnown.model[indexPath.row] {
            bind(cellValue, cell: cell, indexPath: indexPath)
        }
        
        return cell
    }
}

extension PostView : UIScrollViewDelegate {

    /// technically a UITableViewDelegate _is a_ UIScrollViewDelegate, but it
    /// clearer to separate what belongs to the scrolView from what belongs
    /// to the tableView.

    /// This is so that when the table free scrolls in an intertial move it
    /// stops at cell boundaries, never displaying an incomplete cell.

    func scrollViewWillEndDragging(
        scrollView:                 UIScrollView
        ,   withVelocity velocity:      CGPoint
        ,   targetContentOffset:        UnsafeMutablePointer<CGPoint>) {

        targetContentOffset.memory = scrollView.snapToIntegerRowForProposedContentOffset(
            targetContentOffset.memory
        ,   rowHeight:  self.fixedRowHeight())
    }
}