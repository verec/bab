//
//  PostCellView.swift
//  Babylon
//
//  Created by verec on 07/08/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

import QuartzCore

class PostCellView: UIView {

    var autoExpand: (()->())?
    var collapsedHeight: (()->(CGFloat))?

    let logo    =   UIImageView()
    let plch    =   UIActivityIndicatorView(activityIndicatorStyle: .Gray)

    let titl    =   UILabel()
    let cmby    =   UILabel()
    let post    =   UITextView()

    let info    =   PostUserInfo()
    let user    =   UITextView()

    var cell: Model.PostCell? {
        didSet {
            applyCell()
            self.setNeedsLayout()
        }
    }

    convenience init() {
        self.init(frame:CGRect.zero)
    }

    override init(frame:CGRect) {
        super.init(frame:frame)

        setup()

        let tapper = UITapGestureRecognizer()
        self.addGestureRecognizer(tapper)
        tapper.addTarget(self, action: #selector(PostCellView.tapped(_:)))

        self.setNeedsLayout()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension PostCellView {

    @objc func tapped(reco: UITapGestureRecognizer) {

        if reco.state == .Ended {
            guard let cell = self.cell else {
                return
            }
            let point = reco.locationInView(self)

            if logo.frame.contains(point) {
                cell.userVisible = !cell.userVisible
                cell.bodyVisible = false
                self.autoExpand?()
            } else {
                /// the whole remaining area of the cell is fair game
                cell.bodyVisible = !cell.bodyVisible
                cell.userVisible = false
                self.autoExpand?()
            }
        }
    }
}

extension PostCellView {

    func setup() {
        self.addSubview(logo)
        self.addSubview(plch)

        self.addSubview(titl)
        self.addSubview(cmby)
        self.addSubview(post)

        self.addSubview(info)
        self.addSubview(user)

        setupLabel(titl, font: Fonts.Post.titleCellFont, color: UIColor(white: 0.4, alpha: 1.0))
        setupLabel(cmby, color: UIColor(white: 0.3, alpha: 1.0))
//        setupLabel(post, color: UIColor(white: 0.5, alpha: 1.0))
    }

    func setupLabel(label: UILabel, text: String? = .None, font: UIFont? = .None, color: UIColor? = .None) {

        if let text = text      {   label.text = text       }
        if let font = font      {   label.font = font       }
        if let color = color    {   label.textColor = color }

        label.backgroundColor = UIColor.clearColor()
        label.textAlignment = .Left
        label.numberOfLines = 0
        label.lineBreakMode = .ByTruncatingTail
    }
}

extension PostCellView {

    func applyCell() {
        guard let cell = self.cell else {
            return
        }

        let email = cell.user.email

        self.titl.text = cell.post.title

        let cmntCount = cell.cmts.count

        var ats = NSAttributedString.seed()
            .append("\(cell.user.username)", font: Fonts.Post.byCellFont, color: Colors.Post.byText)

        if cmntCount > 0 {
            ats = ats
                .append(", ",           font: Fonts.Post.byCellFont,            color: Colors.Post.byText)
                .append("\(cmntCount)", font: Fonts.Post.commentCountCellFont,  color: Colors.Post.commentCountText)
                .append(" comments.",   font: Fonts.Post.byCellFont,            color: Colors.Post.byText)
        }

        cmby.attributedText = ats

        if let image = WellKnown.avatarCache.cache[email] {
            self.logo.image = image
            self.logo.bounds = CGRect.zero.size(CGSize(width: 60.0, height: 60.0))
        } else {
            self.logo.image = nil
            self.logo.bounds = CGRect.zero
            WellKnown.avatarCache.load(email) {
                (image) in

                if let image = image {
                    self.logo.image = image
                    self.logo.bounds = CGRect.zero.size(CGSize(width: 60.0, height: 60.0))
                    self.setNeedsLayout()
                    self.setNeedsDisplay()
                }
            }
        }

        if cell.bodyVisible {
            let bodyText = cell.post.body.nonl
            var ats = NSAttributedString.seed()
                .append(bodyText,           font: Fonts.Post.postCellFont,  color: Colors.Post.postText)

            for cmt in cell.cmts {
                let email = cmt.email

                ats = ats
                    .append("\n\n\(email): ",  font: Fonts.Post.postByCellFont,   color: Colors.Post.byText)
                    .append(cmt.body.nonl,        font: Fonts.Post.commentCellFont,  color: Colors.Post.commentText)

            }

            post.attributedText = ats
        } else {
            post.attributedText = nil
        }

        if cell.userVisible {
            let ats = NSAttributedString.seed()
                .append("\(cell.user.name)",                font: Fonts.Post.userCellFont, color: Colors.Post.userText)
                .append("\n\(cell.user.address.street)",    font: Fonts.Post.userCellFont, color: Colors.Post.userText)
                .append("\n\(cell.user.address.city)",      font: Fonts.Post.userCellFont, color: Colors.Post.userText)
                .append("\n\(cell.user.address.zipcode)",   font: Fonts.Post.userCellFont, color: Colors.Post.userText)
                .append("\n\n\(cell.user.website)",         font: Fonts.Post.webCellFont,  color: Colors.Post.userText)
            user.attributedText = ats
        } else {
            user.attributedText = nil
        }

        user.backgroundColor = UIColor.clearColor()
        user.layer.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.5).CGColor
        user.layer.cornerRadius = 16.0
        user.layer.masksToBounds = true
        user.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 8.0)
        user.textAlignment = .Center

        user.editable = false
        user.selectable = false

        post.backgroundColor = UIColor.clearColor()
        post.editable = false
        post.selectable = false

        self.setNeedsLayout()
        self.setNeedsDisplay()
    }
}

extension PostCellView {

    override func layoutSubviews() {
        super.layoutSubviews()

        guard let cell = cell else {
            return
        }

        info.cell = cell

        if !self.bounds.isDefined {
            return
        }

        let logoView:UIView
        if let _ = logo.image {
            plch.stopAnimating()
            plch.hidden = true
            logo.hidden = false
            logoView = logo
        } else {
            plch.hidden = false
            logo.hidden = true
            plch.startAnimating()
            logoView = plch
        }

        var layoutBounds = self.bounds
        if let collapsedHeight = collapsedHeight {
            layoutBounds.size.height = collapsedHeight()
        }

        logoView.frame = CGRect.zero.square(40.0).centered(intoRect: layoutBounds)
            .edge(.Left, alignedToRect: layoutBounds)
            .edge(.Left, offsetBy: -10.0)

        let columns = layoutBounds.tabStops([0.25, 0.71])

        titl.bounds.size = titl.sizeThatFits(columns[0].size)
        cmby.bounds.size = cmby.sizeThatFits(columns[0].size)
        titl.frame = titl.bounds.positionned(intoRect: layoutBounds, widthUnitRange: 0.5, heightUnitRange: 0.35)
            .edge(.Left, alignedToRect: columns[0])
            .edge(.Top, alignedToRect: logoView.frame)
        cmby.frame = cmby.bounds.positionned(intoRect: layoutBounds, widthUnitRange: 0.5, heightUnitRange: 0.80)
            .edge(.Right, alignedToRect: columns[0])

        let expandedBounds = self.bounds.edge(.Top, shrinkBy: layoutBounds.size.height)

        post.alpha = cell.bodyVisible ? 1.0 : 0.0
        info.alpha = cell.userVisible ? 1.0 : 0.0
        user.alpha = cell.userVisible ? 1.0 : 0.0

        info.frame = expandedBounds

        post.bounds.size = post.sizeThatFits(expandedBounds.size)

        post.frame.origin.x = 7.0
        post.frame.origin.y = 0.0 + expandedBounds.minY
        /// TODO: stop trial & error and actually compute the line height!
        post.frame.size.height = expandedBounds.size.height - 18.0

        let left = expandedBounds.slice(slice: 0, outOf: 2, aspect: .Tall).insetBy(dx: 24.0, dy: 24.0)
        user.frame = left
    }
}