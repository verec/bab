//
//  AppView.swift
//  Babylon
//
//  Created by verec on 07/08/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

/// Responsible for maintaining the layout of whichever top level subviews are
/// visible as the user `navigates` from "page" to "page"

class AppView : UIView {

    let caption: UILabel = {
        let label = $0
        label.text = "Wordlwide Conversation"
        label.font = Fonts.Post.captionFont
        label.textColor = Colors.Post.captionText
        label.textAlignment = .Center
        return label
    }(UILabel())
}

extension AppView {

    func setBusy(busy:Bool) {
        GCD.MainQueue.async {
            if busy {
                Views.globalWaitSpinner.startAnimating()
            } else {
                Views.globalWaitSpinner.stopAnimating()
            }
            self.setNeedsLayout()
        }
    }
}

extension AppView {

    func addSubviewsIfNeeded() {

        if !Views.globalWaitSpinner.orphaned {
            return
        }

        self.addSubview(Views.globalWaitSpinner)
        self.addSubview(Views.Post.postView)
        self.addSubview(caption)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        if !self.bounds.isDefined {
            return
        }

        addSubviewsIfNeeded()

        /// layout the post, whole screen
        let captionRect = self.bounds.top(40.0)
        caption.sizeToFit()
        caption.frame = caption.bounds.centered(intoRect: captionRect)
            .offsetBy(dx: 0, dy: 32.0)
        Views.Post.postView.frame = self.bounds.shrink(.Top, by: 40.0)

        /// end up with the spinner: if visible, must be on top
        if Views.globalWaitSpinner.hidden {
            return
        }
        self.bringSubviewToFront(Views.globalWaitSpinner)

        Views.globalWaitSpinner.frame = CGRect.zero.square(40.0).centered(intoRect: self.bounds)
    }
}