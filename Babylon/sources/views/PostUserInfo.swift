//
//  PostUserInfo.swift
//  Babylon
//
//  Created by verec on 07/08/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class PostUserInfo: UIView {

    let mapView = MKMapView()

    var cell: Model.PostCell? {
        didSet {
            applyCell()
            self.setNeedsLayout()
        }
    }
}

extension PostUserInfo {
    func applyCell() {
        guard let cell = self.cell else {
            return
        }

        let location = cell.user.address.geo
        mapView.setCenterCoordinate(location, animated: false)
    }
}

extension PostUserInfo {

    override func layoutSubviews() {
        super.layoutSubviews()

        if !self.bounds.isDefined {
            return
        }

        if mapView.orphaned {
            self.addSubview(mapView)
        }

        mapView.frame = bounds
    }
}