//
//  PostCell.swift
//  Babylon
//
//  Created by verec on 07/08/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

class PostCell: UITableViewCell {

    let view = PostCellView()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .Default, reuseIdentifier: reuseIdentifier)

        super.backgroundColor = UIColor.clearColor()
        self.contentView.addSubview(view)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension PostCell {

    override func prepareForReuse() {
        super.prepareForReuse()
        view.logo.image = nil
    }
}

extension PostCell {

    override func setSelected(selected: Bool, animated: Bool) {
        /// we handle the selection ourselves.
    }

    override func setHighlighted(highlighted: Bool, animated: Bool) {
        /// we handle the hilight ourself
    }
}

extension PostCell {

    override func layoutSubviews() {

        super.layoutSubviews()

        if !self.contentView.bounds.isDefined {
            return
        }

        view.frame = self.contentView.bounds
    }
}
