//
//  Extensions.swift
//  Babylon
//
//  Created by verec on 05/08/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

extension String {

    var asDouble: Double? {
        return Double(self)
    }

    var NS: NSString {
        return self as NSString
    }

    var nonl: String {
        let justWords = self.NS.componentsSeparatedByCharactersInSet(NSCharacterSet.newlineCharacterSet()).map {
            $0.NS.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        }
        return (justWords as NSArray).componentsJoinedByString(" ") as String
    }
}

extension UIView {

    var orphaned:Bool {
        return self.superview == nil
    }
}

extension UIScrollView {

    func snapToIntegerRowForProposedContentOffset(targetContentOffset: CGPoint, rowHeight: CGFloat) -> CGPoint {
        let dest = targetContentOffset
        let high = self.bounds.size.height
        let span = self.contentSize.height - high
        let oneRow = rowHeight
        let halfRow = oneRow / 2.0
        var row = floor(dest.y / oneRow)
        let rem = dest.y - row * oneRow
        if rem > halfRow {
            row += 1.0
        }
        var target = row * oneRow
        if target > span {
            target = span
        }
        return CGPoint(x: dest.x, y: target)
    }
}

extension NSAttributedString {

    class func seed() -> NSAttributedString {
        return NSAttributedString()
    }

    func append(text:String, font:UIFont, color:UIColor, bgColor:UIColor? = .None) -> NSAttributedString {

        let m:NSMutableAttributedString = NSMutableAttributedString(attributedString: self)

        let n:NSMutableAttributedString = NSMutableAttributedString(string: text)
        let r = NSRange(location: 0, length: n.length)
        n.addAttribute(NSFontAttributeName, value: font, range: r)
        n.addAttribute(NSForegroundColorAttributeName, value: color, range: r)
        if let bgColor = bgColor {
            n.addAttribute(NSBackgroundColorAttributeName, value: bgColor, range:r)
        }

        m.appendAttributedString(n)
        return m
    }
}
