//
//  WellKnown.swift
//  Babylon
//
//  Created by verec on 05/08/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation

struct WellKnown {
    static let network      = BasicRestGET()

    static var model        = Model()
    static var avatarCache  = AvatarCache()
}