//
//  Colors.swift
//  Babylon
//
//  Created by verec on 07/08/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

struct Colors {

    struct Primitives {
        static let lightGray            =   UIColor(white: 0.95, alpha: 1.0)
        static let darkGray             =   UIColor(white: 0.35, alpha: 1.0)
        static let darkGray2            =   UIColor(white: 0.27, alpha: 1.0)
        static let darkGray3            =   UIColor(white: 0.22, alpha: 1.0)
        static let darkerGray           =   UIColor(white: 0.15, alpha: 1.0)
    }

    struct Global {
        static let  background          =   Primitives.lightGray
    }

    struct Post {
        static let  captionText         =   Primitives.darkGray
        static let  byText              =   Primitives.darkGray
        static let  postText            =   Primitives.darkGray2
        static let  commentText         =   Primitives.darkGray3
        static let  commentCountText    =   Primitives.darkerGray
        static let  userText            =   Primitives.lightGray
    }
}