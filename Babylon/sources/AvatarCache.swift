//
//  AvatarCache.swift
//  Babylon
//
//  Created by verec on 07/08/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

class AvatarCache {

    typealias ImageCompletion = (UIImage?)->()

    /// it is essential that these two properties always be accessed from the
    /// main thread
    var cache = [String:UIImage]()
    var reqst = [String:[ImageCompletion]]()

    func load(name:String, size:Int = 60, uiCompletion:ImageCompletion) {

        /// this is required to avoid having to explicitely use critical sections
        assert(NSThread.isMainThread())

        if let image = cache[name] {
            /// this branch is almost never executed, as the load requests are
            /// queued faster than the newtwork has time to complete.
            GCD.MainQueue.async {
                print("image found in cache for \(name)")

                uiCompletion(image)
            }
            return
        }



        if var compl = reqst[name] {
            compl.append(uiCompletion)
            reqst[name] = compl
            /// since a request is still pending, no need to go again on the
            /// network
            return
        } else {
            reqst[name] = [uiCompletion]
        }

        let fullURL = NSURL(string: "https://api.adorable.io/avatars/\(size)/\(name)")
        WellKnown.network.load(fullURL: fullURL) {
            (error, data) in
            if let data = data {
                GCD.MainQueue.async {
                    if let image = UIImage(data: data) {
                        print("caching image for \(name)")
                        self.cache[name] = image

                        if let compl = self.reqst[name] {
                            for c in compl {
                                c(image)
                            }
                            self.reqst[name] = nil
                        }
                    }
                }
            }
        }
    }
}