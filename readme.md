### Coding Exercise  
    Using the API endpoint  
    
    GET http://jsonplaceholder.typicode.com/posts  
    GET http://jsonplaceholder.typicode.com/users  
    GET http://jsonplaceholder.typicode.com/comments  
  
    Save the results of the request using Core Data (or another local database)   
    so that the app can still be used (after first successful connection) offline.  
    
    create a simple iOS app in Swift with two screens:  
    
    Screen1:   
    a title  
    a tableview. Tapping a cell should take you to Screen2; each cell displaying:  
      - post title  
    
    Screen2:  
    back button back to screen 1  
    title  
    labels for the fields:  
      - post title  
      - post body  
      - user name  
      - number of comments  
  
    Bonus:  
    
    Based on the user’s email address, use http://avatars.adorable.io to generate  
    a unique avatar for each of the user. To be shown on both screens.  
    
    Tapping on user name should take you to a third screen with all user information  
    including a map of their location based on lat and long values.  
   
### Screencast

[A quick tour of the app showing what the app looks like](https://www.dropbox.com/s/adms2lxazynss85/BBCodingCast.mov?dl=0)  

### Analysis

The above requirements are prescriptive in terms of implementation, suggesting a combination of storyboards and view controllers coupled with a standard "master detail" navigation. However a high level analysis of the workflows reveals a simpler architecture that doesn't lose the user mental context by switching screens and hence obviates both the need of multiple screens and some form of going back.

By gathering the pieces of data as the user percieves them, and taking into account the totality of the required feature set, we arrive at a slightly different model.  

### User Stories  

- _as a user I can see a scrollable list of post titles together withe the poster avatar_  
- _as a user I can tap on the post title to reveal the post contents and the number of comments_  
- _as a user I can tap on the poster avatar and see details about the poster including a map of their location_  

### Workflows

- access the list of all posts across all users in a single view made of post title rows, with a user avatar
- when the user avatar is pressed, the cell expands to reveal user details & map
- when a post title is pressed, the cell expands to reveal the post details, body, comment count, user name

This keeps everything _in place_, offering the user details and/or post details _without_ switching context.  

### Design

- value types for the JSON model `User`, `Post`, `Comment`
- reference type for the table model `PostCell`
- Manual Layout
- struct for most functions unless directly interfacing with `UIKit`

### Storage

Uses flat files stored in the Documents folder the first time through.
JSON is parsed every time the app is launched, whether the data comes from the local storage or a network connection.
Timing shows it to be very fast: less than 4ms for each of the 3 JSON files or 12ms total.
This simplifies the code a lot.

### Rationale

See my blog posts about [WellKnown](http://tinyurl.com/zylaeh2) and also about the [Single View Controller](http://tinyurl.com/jgyeknm)   

Also, see [YAGNI](https://en.wikipedia.org/wiki/You_aren%27t_gonna_need_it), [KISS](https://en.wikipedia.org/wiki/KISS_principle) and [MoSCoW](https://en.wikipedia.org/wiki/MoSCoW_method)

Rather than MVC or MVVM, this uses a split between mechanisms and policies, letting the _views_ be  _views_, and spreading the _controller_ apart at higher levels.

This results in a few more classes or structs but which are much smaller and manageable since they strive to do one thing only, but one thing well.

Often the construction part relies on the inherited defaulst (mostly for `UIView`s) and this is just fine.
The layout is performed within `layoutSubviews` overrides, _as it should be and was designed to be_  

### Hilights

`Json.parse` is generic and returns the appropriate array based on the _left_-hand sinde of the assignment (generic on the `return type`)
`WellKnown` gathers objects that are referred to from views and logic elsehwre but are otherwise _well known_ inside the app

### Unit Tests

For JSON parsing  


