//
//  BabylonTests.swift
//  BabylonTests
//
//  Created by verec on 05/08/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//
import Foundation
import XCTest

@testable import Babylon

class BabylonTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testBundle() -> NSBundle {
        return NSBundle(forClass: BabylonTests.self)
    }


    func testLoadUsers() {
        func loadUsers() -> [Json.User]? {
            if let data = Json.bundleLoad("users", inBundle: testBundle())
            ,  let json = Json.decode(data)
            ,  let users:[Json.User]? = Json.parse(json) {

            return users
            }
            return .None
        }

        if let users = loadUsers() {
            XCTAssert(users.count == 10, "wrong number of users")
        } else {
            XCTAssert(false, "couldn't load users")
        }
    }

    func testLoadPosts() {
        func loadPosts() -> [Json.Post]? {
            if let data = Json.bundleLoad("posts", inBundle: testBundle())
            ,  let json = Json.decode(data)
            ,  let posts:[Json.Post]? = Json.parse(json) {

                return posts
            }
            return .None
        }

        if let posts = loadPosts() {
            XCTAssert(posts.count == 100, "wrong number of posts")
        } else {
            XCTAssert(false, "couldn't load posts")
        }
    }

    func testLoadComments() {
        func loadComments() -> [Json.Comment]? {
            if let data = Json.bundleLoad("comments", inBundle: testBundle())
            ,  let json = Json.decode(data)
            ,  let comments:[Json.Comment]? = Json.parse(json) {

                return comments
            }
            return .None
        }

        if let comments = loadComments() {
            XCTAssert(comments.count == 500, "wrong number of comments")
        } else {
            XCTAssert(false, "couldn't load posts")
        }
    }
}
